package simpleclient;

import simpleclient.client.hook.HookManager;
import simpleclient.client.hook.IHookProvider;
import simpleclient.client.hook.jaggl.GLHookProvider;
import simpleclient.client.hook.jaggl.GLImageDef;
import simpleclient.client.hook.jaggl.IncHeap;
import simpleclient.client.hook.jaggl.OpenGLDetour;
import simpleclient.plugin.Plugin;

import javax.swing.*;
import java.awt.*;
import java.util.List;
import java.util.concurrent.Semaphore;

import static simpleclient.client.hook.jaggl.GLHookProvider.*;
import static simpleclient.client.hook.jaggl.GLHookProvider.ITEM_HEIGHT;

public class Client {
    private static final Semaphore loadMutex = new Semaphore(1);
    public static boolean debugModels, debugInventory;
    private static Plugin currentPlugin;

    static {
        try {
            loadMutex.acquire();
        } catch (InterruptedException ignored) {
        }
        HookManager.addHookExecutor(new HookManager.IHookExecutor() {
            @Override
            public void hookEnabled(IHookProvider provider) {
                loadMutex.release();
                System.out.println(provider.getClass().getName() + " enabled.");
            }

            @Override
            public void hookDisabled(IHookProvider provider) {
                loadMutex.tryAcquire();
                System.out.println(provider.getClass().getName() + " disabled.");
            }

            @Override
            public void doPaint(Graphics g2d) {
                if (currentPlugin == null)
                    return;



                g2d.setFont(new Font("Consolas", Font.PLAIN, 11));

                g2d.setColor(Color.GREEN);

                IncHeap inc = OpenGLDetour.imageBuckets[ITEM_WIDTH][ITEM_HEIGHT];
                for(int i = 0; i < inc.head; i++) {
                    GLImageDef img = (GLImageDef) inc.get(i);
                    Rectangle rec = img.bounds();
                    g2d.drawRect(rec.x, rec.y, rec.width, rec.height);
                }

                currentPlugin.onPaint(g2d);
            }
        });
    }

    public static Point getMouse() {
        Point ptr = MouseInfo.getPointerInfo().getLocation();
        SwingUtilities.convertPointFromScreen(ptr, BootLoader.APPLET);
        return ptr;
    }

    static void load(Plugin plugin) {
        try {
            loadMutex.acquire();
        } catch (InterruptedException ignored) {
        }
        System.out.println("Loading plugin " + plugin.getClass().getName());
        (currentPlugin = plugin).onEnable();
        System.out.println("\tLoaded");
        loadMutex.release();
    }

    static void quit(Plugin plugin) {
        plugin.onDisable();
    }
}
