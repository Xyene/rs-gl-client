package simpleclient.plugin;

import java.awt.*;

public abstract class Plugin implements Runnable {
    public void onEnable() {
    }

    public void onDisable() {
    }

    public void onSuspend() {
    }

    //private static final Font DISPLAY_FONT = new Font("Consolas", Font.PLAIN, 13);
    public void onPaint(Graphics g2d) {
    }
}
