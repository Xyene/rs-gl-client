package simpleclient.plugin;

import java.awt.*;
import java.util.concurrent.Semaphore;

public class ScriptContainer {
    private final Plugin plugin;
    private Thread executionThread;
    private Semaphore stoplight = new Semaphore(1);

    public ScriptContainer(Plugin plugin) {
        this.plugin = plugin;
    }

    public void run() {
        executionThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (true) {
                    try {
                        stoplight.acquire();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    plugin.run();
                    stoplight.release();
                }
            }
        });
        executionThread.start();
    }

    public void stop() {
        try {
            executionThread.stop();
        } catch (Exception /* I think always ThreadDeath */ ignored) {
        }
    }

    public void suspend(boolean flag) {
        if (flag) {
            try {
                stoplight.acquire();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        } else {
            stoplight.release();
        }
    }

    public void paint(Graphics2D graphics) {
        plugin.onPaint(graphics);
    }

    public Plugin getPlugin() {
        return plugin;
    }
}
