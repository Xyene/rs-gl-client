package simpleclient;

import simpleclient.debug.TestPlugin;
import simpleclient.ui.ClientFrame;

import javax.swing.*;
import java.applet.Applet;
import java.io.IOException;
import java.util.Arrays;

public class BootLoader {
    public static Applet APPLET;
    static ClientFrame CLIENT;

    public static void main(String[] argv) throws IOException, ReflectiveOperationException, UnsupportedLookAndFeelException {
        boolean oldschool = Arrays.asList(argv).contains("-oldschool");

        // We are really bad people who create components outside of the EDT
        System.setProperty("insubstantial.checkEDT", "false");
        System.setProperty("insubstantial.logEDT", "false");
        //UIManager.setLookAndFeel("org.pushingpixels.substance.api.skin.SubstanceGraphiteGlassLookAndFeel");
        UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());

        JPopupMenu.setDefaultLightWeightPopupEnabled(false); // Since java.awt.Canvas is heavyweight
        JFrame.setDefaultLookAndFeelDecorated(true);
        JDialog.setDefaultLookAndFeelDecorated(true);

        CLIENT = ClientFrame.spawn(oldschool, 2);

        TestPlugin plugin = new TestPlugin();
        Client.load(plugin);
    }
}
