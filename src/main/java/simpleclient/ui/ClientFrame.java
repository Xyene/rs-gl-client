package simpleclient.ui;

import simpleclient.BootLoader;
import simpleclient.Client;
import simpleclient.client.browser.Browser;

import javax.swing.*;
import java.applet.Applet;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class ClientFrame extends JFrame {
    private Applet applet;
    private JPanel appletPanel = new JPanel(new BorderLayout());
    private JMenu input = new JMenu("Input");
    private JMenu view = new JMenu("View");
    private JPanel main = new JPanel(new BorderLayout());
    private JPanel statusBar = new JPanel(new BorderLayout());
    private MemoryMonitorProgressBar memoryMonitor = new MemoryMonitorProgressBar();

    ClientFrame(final boolean oldschool, final int world) throws IOException {
        super("RuneScape");
        //applet.setMinimumSize(new Dimension(1, 1));
        setLayout(new BorderLayout());

        main.add(appletPanel, BorderLayout.CENTER);
        statusBar.add(memoryMonitor, BorderLayout.EAST);

        main.add(statusBar, BorderLayout.SOUTH);
        getContentPane().add(main, BorderLayout.CENTER);

        final Browser io = Browser.newSession();

        JLabel loadingLabel = new JLabel(new ImageIcon(
                io.getBytes("http://www.runescape.com/img/game/splash.gif")
        ), JLabel.CENTER);
        JLabel loadingNotifLabel = new JLabel("Downloading applet...");
        loadingNotifLabel.setFont(loadingNotifLabel.getFont().deriveFont(20f));
        loadingNotifLabel.setForeground(Color.WHITE);

        loadingLabel.setPreferredSize(new Dimension(800, 600));

        appletPanel.setBackground(Color.BLACK);
        appletPanel.add(loadingNotifLabel, BorderLayout.SOUTH);
        appletPanel.add(loadingLabel, BorderLayout.CENTER);

        pack();

        new Thread("RuneScape Applet Loader") {
            @Override
            public void run() {
                try {
                    Applet applet = io.getApplet(oldschool, world);
                    BootLoader.APPLET = applet;
                    applet.setPreferredSize(new Dimension(800, 600));
                    applet.setMinimumSize(new Dimension(1, 1));
                    appletPanel.removeAll();
                    appletPanel.add(applet, BorderLayout.CENTER);

                    revalidate();

                    applet.init();
                    applet.start();
                } catch (IOException | ReflectiveOperationException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }

    public static ClientFrame spawn(boolean oldschool, int world) throws IOException {
        ClientFrame  client = new ClientFrame(oldschool, world);
        client.setLocationRelativeTo(null);
        client.setVisible(true);
        client.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        return client;
    }
}
