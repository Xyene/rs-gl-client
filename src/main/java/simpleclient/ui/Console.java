package simpleclient.ui;

import javax.swing.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyleContext;
import java.awt.*;
import java.io.*;

public class Console {
    static class RedirectedOutputStream extends OutputStream {
        private final JTextPane displayPane;
        private final Color color;

        public RedirectedOutputStream(JTextPane displayPane, Color color) {
            this.displayPane = displayPane;
            this.color = color;
        }

        @Override
        public void write(final int b) throws IOException {
            updateTextPane(String.valueOf((char) b));
        }

        @Override
        public void write(byte[] b, int off, int len) throws IOException {
            updateTextPane(new String(b, off, len));
        }

        @Override
        public void write(byte[] b) throws IOException {
            write(b, 0, b.length);
        }

        public void updateTextPane(String line) {
            StyleContext sc = StyleContext.getDefaultStyleContext();
            AttributeSet aset = sc.addAttribute(SimpleAttributeSet.EMPTY, StyleConstants.Foreground, color);

            int len = displayPane.getDocument().getLength(); // same value as getText().length();
            displayPane.setCaretPosition(len);  // place caret at the end (with no selection)
            displayPane.setCharacterAttributes(aset, false);
            displayPane.replaceSelection(line);
            displayPane.setCaretPosition(displayPane.getDocument().getLength());
        }
    }

    public static void redirectOutput(JTextPane displayPane) {
        Console.redirectOut(displayPane, Color.BLACK);
        Console.redirectErr(displayPane, Color.RED);
    }

    public static void redirectOut(JTextPane displayPane, Color color) {
        System.setOut(new PrintStream(new RedirectedOutputStream(displayPane, color), true));
    }

    public static void redirectErr(JTextPane displayPane, Color color) {
        System.setErr(new PrintStream(new RedirectedOutputStream(displayPane, color), true));
    }
}