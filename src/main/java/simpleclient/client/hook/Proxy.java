package simpleclient.client.hook;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates that interfaces marked with this annotation are meant to be instantiated
 * automatically by {@link simpleclient.clientloader.ProxyFactory}.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Proxy {
    /**
     * The unqualified getName of the class to proxy.
     *
     * @return The class getName
     */
    String value();
}
