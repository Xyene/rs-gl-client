package simpleclient.client.hook;

import simpleclient.client.hook.jaggl.GLHookProvider;
import simpleclient.client.loader.ProxyFactory;

import java.awt.*;
import java.util.*;
import java.util.List;

public class HookManager {
    private static final List<IHookProvider> HOOK_SERVICE_LOADER = new ArrayList<>();
    private static final LinkedList<IHookExecutor> HOOK_EXECUTORS = new LinkedList<>();
    private static final LinkedList<IHookProvider> HOOK_PROVIDERS = new LinkedList<>();
    private static IHookProvider currentHook;
    private static final IHookExecutor PARENT_HOOK_EXECUTOR = new IHookExecutor() {
        @Override
        public void hookEnabled(IHookProvider provider) {
            currentHook = provider;
            for (IHookExecutor callback : HOOK_EXECUTORS)
                callback.hookEnabled(provider);
        }

        @Override
        public void hookDisabled(IHookProvider provider) {
            currentHook = null;
            for (IHookExecutor callback : HOOK_EXECUTORS)
                callback.hookDisabled(provider);
        }

        @Override
        public void doPaint(Graphics graphics) {
            for (IHookExecutor callback : HOOK_EXECUTORS)
                callback.doPaint(graphics);
        }
    };
    private static LinkedList<Class> DETOURS = new LinkedList<>();
    private static LinkedList<Class> OVERRIDES = new LinkedList<>();

    static {
        GL = new GLHookProvider();
        reloadHooks();
       // HookManager.registerOverride(RSCanvas.class);
    }

    public static List<IHookProvider> getAllHooks() {
        return Collections.unmodifiableList(HOOK_PROVIDERS);
    }

    public static IHookProvider getActiveHook() {
        return currentHook;
    }

    public static <T> T generateProxyFor(Class<T> clazz) {
        return ProxyFactory.getProxyFor(clazz);
    }

    public static List<Class> getAllDetours() {
        return Collections.unmodifiableList(DETOURS);
    }

    public static void registerDetour(Class clazz) {
        DETOURS.add(clazz);
    }

    public static List<Class> getAllOverrides() {
        return Collections.unmodifiableList(OVERRIDES);
    }

    public static void registerOverride(Class clazz) {
        OVERRIDES.add(clazz);
    }

    public static void addHookExecutor(IHookExecutor callback) {
        HOOK_EXECUTORS.add(callback);
    }

    public static boolean removeHookExecutor(IHookExecutor callback) {
        return HOOK_EXECUTORS.remove(callback);
    }

    private static GLHookProvider GL;

    public static void reloadHooks() {
        HOOK_SERVICE_LOADER.clear();
        HOOK_SERVICE_LOADER.add(GL);
        HOOK_PROVIDERS.clear();
        for (IHookProvider hook : HOOK_SERVICE_LOADER) {
            hook.setHookExecutor(PARENT_HOOK_EXECUTOR);
            HOOK_PROVIDERS.add(hook);
        }
    }

    public interface IHookExecutor {
        void hookEnabled(IHookProvider provider);

        void hookDisabled(IHookProvider provider);

        void doPaint(Graphics graphics);
    }
}
