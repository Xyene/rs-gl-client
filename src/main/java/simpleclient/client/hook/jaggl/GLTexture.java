package simpleclient.client.hook.jaggl;

class GLTexture {
    final int textureId;
    final int width, height;
    final long crc;

    GLTexture(int textureId, int width, int height, long crc) {
        this.textureId = textureId;
        this.width = width;
        this.height = height;
        this.crc = crc;
    }

    @Override
    public String toString() {
        return "GLTexture{" +
                "textureId=" + textureId +
                ", width=" + width +
                ", height=" + height +
                ", crc=" + String.format("0x%08X", crc) +
                '}';
    }
}
