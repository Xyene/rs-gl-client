package simpleclient.client.hook.jaggl;


import java.awt.*;

public class GLTexturedQuad {
    GLTexturedQuad() {
    }

    long id;
    Rectangle bounds = new Rectangle();

    public Rectangle bounds() {
        return bounds;
    }

    public long id() {
        return id;
    }

    @Override
    public String toString() {
        return "GLTexturedQuad{" +
                "id=" + id +
                ", bounds=" + bounds +
                '}';
    }
}
