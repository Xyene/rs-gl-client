package simpleclient.client.hook.jaggl;

import simpleclient.client.Reflection;
import simpleclient.client.hook.Detour;
import simpleclient.client.hook.HookManager;
import simpleclient.client.hook.Interceptor;
import simpleclient.client.hook.jaggl.draw.GLGraphics;
import simpleclient.client.hook.jaggl.proxy.GL;

import java.awt.*;
import java.awt.color.ColorSpace;
import java.awt.image.*;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.lang.invoke.MethodHandle;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.zip.CRC32;

import static simpleclient.client.hook.jaggl.GLHookProvider.*;
import static simpleclient.client.hook.jaggl.proxy.GL.*;

@Interceptor("jaggl/OpenGL")
public class OpenGLDetour {
    /**
     * Structure to hold information about the current draw context.
     */
    private static GLDrawContext currentDrawContext = new GLDrawContext();
    /**
     * Hashing algorithm for unique ids.
     */
    private static CRC32 hasher = new CRC32();

    public static IncHeap[][] imageBuckets = new IncHeap[1024][1024];
    public static IncHeap.Invalidator imageBucketInvalidator = new IncHeap.Invalidator();

    static {
        for (int x = 0; x < 1024; x++) {
            for (int y = 0; y < 1024; y++) {
                imageBuckets[x][y] = new IncHeap<>(String.format("%dx%d", x, y), new IncHeap.NewInstance<GLImageDef>(){
                    @Override
                    public GLImageDef newInst() {
                        return new GLImageDef();
                    }
                }, imageBucketInvalidator);
            }
        }
    }

    /**
     * Models currently rendered.
     */
    private static HashSet<GLModel> currentModels = new HashSet<>(100);
    /**
     * The model currently being rendered, if any.
     */
    private static GLModel currentModel;
    /**
     * Marker for if we are currently in a model draw sequence.
     */
    private static boolean onModel = false;
    /**
     * Pointer to the texture for the overlay for OpenGL.
     */
    private static int overlayTexture;
    /**
     * Width and height of the viewport.
     */
    public static int width, height;

    public static GLGraphics glGraphics;
    /**
     * Buffer to store blank buffer the size of (@link overlayBuffer).
     * By using {@link System#arraycopy(Object, int, Object, int, int)} instead of {@link java.util.Arrays#fill(byte[], byte)} to
     * clear the screen buffer, we speed up rendering a lot. Though both O(n), arraycopy is implemented natively.
     */
    private static byte[] VBLANK;
    private static HashMap<Integer, byte[]> buffers = new HashMap<>();
    private static Canvas canvas;
    private static Reflection.MethodContainer _swapBuffers;
//
    @Detour
    public static void glTexImage2Di(int target, int getLevel, int internal, int width, int height, int border, int format, int type, int[] buffer, int usage) {
        gl.glTexImage2Di(target, getLevel, internal, width, height, border, format, type, buffer, usage);
        if (buffer != null) {
            try {
//                BufferedImage buf = new BufferedImage(width, height, BufferedImage.TYPE_4BYTE_ABGR);
//                for (int y = 0; y < height; y++) {
//                    for (int x = 0; x < width; x++) {
//                        buf.setRGB(x, y, buffer[y * width + x]);
//                    }
//                }
//                ImageIO.write(buf, "PNG", new File(currentDrawContext.textureId+".png"));
                //long hash = 0;
                //System.out.println(width * height + "::" + buffer.length);
                int margin = height > 12 ? 12 : 0;
                int top = margin * width;
                int pos = top;
                byte[] binary = new byte[width * height - top];
                for (int y = 0; y < height - margin - 1; y++) {
                    for (int x = 0; x < width; x++) {
                        pos = y * width + x;
                        int argb = buffer[pos];
                        binary[pos] = (byte) ((((argb >> 24) & 0xFF) > 240) ? 1 : 0);
                    }
                }/*
                int R = 0, G = 0, B = 0, K = 0;


                for (int I = height < 12 ? 1 : 12; I < height; I++) {
                    for (int J = 0; J < height; J++, K++) {
                        int pos = I * width + J;
                        int argb = buffer[pos];
                        R += ((argb >> 16) & 0xFF);
                        R += ((argb >> 8) & 0xFF);
                        R += ((argb >> 0) & 0xFF);
                        //    G +=*(BuffPos++);
                        //    B +=*(BuffPos++);
                        hash += ((argb >> 24) & 0xFF) > 240 ? 1 : 0;
                    }
                }
                if (false && K != 0) {
                    int MeanColor = new Color(R / K, G / K, B / K).getRGB(); //((R << 16) & 0x00FF0000) | ((G << 8) & 0x0000FF00) | (B & 0x000000FF)
                    hash ^= MeanColor;
                }*/
                hasher.update(binary);

                long hash = hasher.getValue();

               /* //buffer = resizePixels(buffer, width, height, 100, 100);
                byte[] binary = new byte[buffer.length];

                float cx = ((float) width) / 2;
                float cy = ((float) height) / 2;
                float multiplier = 15;
                float max = (float) (Math.sqrt((0 - cx) * (0 - cx) + (0 - cy) * (0 - cy)) * multiplier);

                float avg = 0;
                for (int i = 0; i < buffer.length; i++) {
                    int x = i % width;
                    int y = i / height;
                    float lum = max - (float) (getY(buffer[i]) * Math.sqrt((x - cx) * (x - cx) + (y - cy) * (y - cy)) * multiplier);
                    binary[i] = (byte) (getY(buffer[i]) > 0 ? 1 : 0);

                    avg += lum;
                }

                avg /= (width * height);
                avg *= 1000;

                hasher.update(binary);

                long hash = hasher.getValue();
                hash += Float.floatToIntBits(round(avg, 100));*/
                //    long hash = ColorDifference.genHash(buffer, width, height);

                TEXTURES[currentDrawContext.textureId] = new GLTexture(currentDrawContext.textureId, width, height, hash);
                hasher.reset();
            } catch (Exception e) {
                System.err.println(buffer.length);
                e.printStackTrace();
            }
        }
    }

    @Detour
    public static void glBindTexture(int target, int getId) {
        gl.glBindTexture(target, getId);
        currentDrawContext.textureId = getId;
    }

    @Detour
    public static void glTexCoord2f(float u, float v) {
        if (currentDrawContext != null) {
            currentDrawContext.vertexCount++;
            currentDrawContext.textureSum += u + v;
        }
        gl.glTexCoord2f(u, v);
    }

    @Detour
    public static void glVertex2i(int x, int y) {
        if (currentDrawContext != null) {
            int numCoords = currentDrawContext.vertexCount;
            if (numCoords <= 4) {
                currentDrawContext.x[numCoords - 1] = x;
                currentDrawContext.y[numCoords - 1] = y;
            }
        }
        gl.glVertex2i(x, y);
    }

    @Detour
    public static void glEnd() {
        GLTexture def = TEXTURES[currentDrawContext.textureId];
        if (def != null && currentDrawContext.vertexCount == 4) {
            GLImageDef item = (GLImageDef) imageBuckets[def.width][def.height].next();
            item.bounds.x = (int) currentDrawContext.x[0];
            item.bounds.y = (int) currentDrawContext.y[0];
            item.bounds.width = def.width;
            item.bounds.height = def.height;
            item.id = def.crc;
        }

        // There may still be more commands in this draw context
        // Arrays.fill(currentDrawContext.x, 0);
        // Arrays.fill(currentDrawContext.y, 0);
        currentDrawContext.vertexCount = 0;
        currentDrawContext.textureSum = 0;

        gl.glEnd();
    }

    @Detour
    public static void glVertexPointer(int size, int type, int stride, long pointer) {
        onModel = true;
        currentModel = new GLModel();
        currentModel.stride = stride;
        currentModel.pointer = pointer;

        currentModels.add(currentModel);

        if (false && size == 3 && type == GL_FLOAT)
            try {
                int ptr = (int) pointer;
                byte[] data = buffers.get(currentDrawContext.bufferId);
//                byte[] vertices = new byte[data.length];
                if (data != null) {
                    hasher.update(data, (int) pointer, data.length);
                    currentModel.id = hasher.getValue();
                    hasher.reset();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        else currentModel.id = -1;
        gl.glVertexPointer(size, type, stride, pointer);
    }

    @Detour
    public static void glMultiTexCoord2f(int target, float u, float v) {
        gl.glMultiTexCoord2f(target, u, v);
        if (target == GL_TEXTURE0) {
            // System.out.println("Compass: " + u + ", " + v);
        }
    }

    @Detour
    public static void glTexCoordPointer(int size, int type, int stride, long pointer) {
        gl.glTexCoordPointer(size, type, stride, pointer);
    }

    @Detour
    public static void glBindBufferARB(int target, int getId) {
        currentDrawContext.bufferId = getId;
        gl.glBindBufferARB(target, getId);
    }

    @Detour
    public static void glPushMatrix() {
//        gl.glPushMatrix();
        gl.glPushMatrix();
    }

    @Detour
    public static void glLoadIdentity() {
        //System.out.println("glLoadIdentity()");
        gl.glLoadIdentity();
        if (matMode == GL_MODELVIEW) {
//            System.arraycopy(new float[]{
//                    1.0f, 0.0f, 0.0f, 0.0f,
//                    0.0f, 1.0f, 0.0f, 0.0f,
//                    0.0f, 0.0f, 1.0f, 0.0f,
//                    0.0f, 0.0f, 0.0f, 1.0f},0,modelViewMatrixBuffer,0,16);
        }
    }

    static int matMode;

    @Detour
    public static void glMatrixMode(int mode) {
        //System.out.println("glMatrixMode("+mode+")");
        gl.glMatrixMode(mode);
        matMode = mode;
    }

    @Detour
    public static void glLoadMatrixf(float[] matrix, int usage) {
        //  System.out.println("glLoadMatrixf(" + Arrays.toString(matrix) + ", " + usage + ")");
//if(matMode == GL_MODELVIEW) for(int i = 0; i < 16; i++) matrix[i] *= 1.2f;
        if (matMode == GL_PROJECTION) projectionMatrixBuffer = Arrays.copyOf(matrix, 16);
//        if (matMode == GL_MODELVIEW) {
//            System.arraycopy(matrix, 0, modelViewMatrixBuffer, 0, 16);
//        }
        gl.glLoadMatrixf(matrix, usage);
    }

    @Detour
    public static void glMultMatrixf(float[] matrix, int usage) {
//        //if(matMode == GL_MODELVIEW) System.out.println("glMultMatrixf(" + Arrays.toString(matrix) + ", " + usage + ")");
//        if(matMode == GL_MODELVIEW)
//        {
//            float[] temp = new float[16];
//            float[] a = modelViewMatrixBuffer;
//            float[] b = matrix;
//            float[] r = temp;
//            for (int i = 0; i < 4; i++) {
//                for (int j = 0; j < 4; j++) {
//                    r[i * 4 + j] =
//                            a[i * 4 + 0] * b[0 * 4 + j] + a[i * 4 + 1] * b[1 * 4 + j] + a[i * 4 + 2] * b[2 * 4 + j] + a[i * 4 + 3] * b[3 * 4 + j]
//                    ;
//                }
//            }
//            System.arraycopy(temp,0,modelViewMatrixBuffer,0,16);
//        }
        gl.glMultMatrixf(matrix, usage);
        if (matMode == GL_MODELVIEW) {

            System.arraycopy(matrix, 0, modelViewMatrixBuffer, 0, 16);
            //gl.glGetFloatv(GL_MODELVIEW_MATRIX, modelViewMatrixBuffer, 0);
        }
    }

    @Detour
    public static void glShaderSource(int paramInt, String paramString) {
        File f = new File(paramInt + ".sh");
        try {
            f.createNewFile();
            DataOutputStream dos = new DataOutputStream(new FileOutputStream(f));
            dos.writeBytes(paramString);
            dos.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        gl.glShaderSource(paramInt, paramString);
    }

    @Detour
    public static void glBufferDataARBub(int target, int size, byte[] data, int getOffset, int length) {
        if (data != null) {
            //buffers.put(currentDrawContext.bufferId, data);
        }
        gl.glBufferDataARBub(target, size, data, getOffset, length);
    }

    @Detour
    public static void glDrawElements(int mode, int count, int type, long indices) {
        if (onModel) {
            //currentModel.getId = TEXTURES.containsKey(currentDrawContext.textureId) ? TEXTURES.get(currentDrawContext.textureId).crc : Integer.MIN_VALUE;
            //System.out.println(mode);

            currentModel.triangleCount += count / currentModel.stride;
        }
        gl.glDrawElements(mode, count, type, indices);
    }

    private static float[] modelViewMatrixBuffer = new float[16];
    private static float[] projectionMatrixBuffer = new float[16];
    private static float[] viewBuffer = new float[3];

    @Detour
    public static void glPopMatrix() {
        // System.out.println("glPopMatrix()");
        if (onModel) {
            // gl.glGetFloatv(GL_MODELVIEW_MATRIX, modelViewMatrixBuffer, 0);
            // gl.glGetFloatv(GL_PROJECTION_MATRIX, projectionMatrixBuffer, 0);

            float modelMatrix[] = new float[16];
            System.arraycopy(modelViewMatrixBuffer, 0, modelMatrix, 0, 16);

            gl.glGetFloatv(GL_MODELVIEW_MATRIX, modelViewMatrixBuffer, 0);
            gl.glGetFloatv(GL_PROJECTION_MATRIX, projectionMatrixBuffer, 0);


            if (GLU.gluProject(0, 0, 0, modelViewMatrixBuffer, projectionMatrixBuffer, new int[]{0, 0, width, height}, viewBuffer)) {
                currentModel.screenX = (int) viewBuffer[0];
                currentModel.screenY = height - (int) viewBuffer[1] - 1;
            }

            int[] viewport = new int[4];
            gl.glGetIntegerv(GL_VIEWPORT, viewport, 0);
            float[] coords = new float[3];
            GLU.gluUnProject(
                    viewBuffer[0], viewBuffer[1], // Model x, y on screen
                    1.0f,
                    modelMatrix, projectionMatrixBuffer, new float[]{viewport[0], viewport[1], viewport[2], viewport[3]},
                    coords
            );
            currentModel.x = coords[0];
            currentModel.y = coords[1];
            currentModel.z = coords[2];
            float x = currentModel.x;
            float y = currentModel.y;
            float z = currentModel.z;
            float m = (float) Math.sqrt(x * x + y * y + z * z);
            x /= m;
            y /= m;
            z /= m;
            currentModel.x = (float) Math.toDegrees(Math.atan(x / -y)); // Yaw
            currentModel.y = (float) Math.toDegrees(Math.atan(Math.sqrt(x * x + y * y)) / z); // Pitch
        }

        onModel = false;
        gl.glPopMatrix();
    }

    @Detour(type = Detour.TargetType.INSTANCE)
    public static long init(Object _this, Canvas paramCanvas, int paramInt1, int paramInt2, int paramInt3, int paramInt4, int paramInt5, int paramInt6) {
        if (gl == null) gl = HookManager.generateProxyFor(GL.class);

        hookEnabled = true;
        canvas = paramCanvas;
        // Update VBLANK
        resizeSurface();
        if (executor != null)
            executor.hookEnabled(instance);
        try {
            bufferHook(_this);
            System.err.println("Initializing Canvas...");
            long a = (long) (Long) init.invoke(_this, paramCanvas, paramInt1, paramInt2, paramInt3, paramInt4, paramInt5, paramInt6);
            System.err.println("...returned " + a);
            return a;
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
        return 0;
    }

    @Detour(type = Detour.TargetType.INSTANCE)
    public static void releaseSurface(Object _this, Canvas paramCanvas, long paramLong) {
        // We reset the buffers since it will cause the current texture getId to be invalidated.
        // If the hook was disabled and reenabled without this, we would get a white screen from the invalid texture.
        canvas = null;
        overlayBuffer = null;
        overlayGraphics = null;
        hookEnabled = false;
        if (executor != null)
            executor.hookDisabled(instance);
        bufferHook(_this);
        try {
            releaseSurface.invoke(_this, paramCanvas, paramLong);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }
    }

    @Detour(type = Detour.TargetType.INSTANCE)
    public static void surfaceResized(Object _this, long paramLong) {
        resizeSurface();
        bufferHook(_this);
        try {
            surfaceResized.invoke(_this, paramLong);
        } catch (Throwable throwable) {

            throwable.printStackTrace();
        }
    }

    private static void resizeSurface() {
        int[] viewport = new int[4];
        gl.glGetIntegerv(GL_VIEWPORT, viewport, 0);

        width = viewport[2];
        height = viewport[3];
        VBLANK = new byte[width * height * 4];
    }

    static MethodHandle swapBuffers;
    static MethodHandle surfaceResized;
    static MethodHandle init;
    static MethodHandle releaseSurface;
    static int thisCanvas;

    private static void bufferHook(Object _this) {
        if (!(_this.hashCode() != thisCanvas || swapBuffers == null || surfaceResized == null || init == null || releaseSurface==null))
            return;
        System.err.println("Reinitializing buffer handles");
        thisCanvas = _this.hashCode();
        swapBuffers = Reflection.declaredMethod("swapBuffers")
                .in(_this)
                .withParameters(long.class).handle();
        releaseSurface = Reflection.declaredMethod("releaseSurface")
                .in(_this)
                .withParameters(Canvas.class, long.class).handle();
        surfaceResized =
                Reflection.declaredMethod("surfaceResized")
                        .in(_this)
                        .withParameters(long.class).handle();
        init = Reflection.declaredMethod("init")
                .in(_this)
                .withReturnType(Long.class)
                .withParameters(Canvas.class, int.class, int.class, int.class, int.class, int.class, int.class).handle();
    }

    @Detour(type = Detour.TargetType.INSTANCE)
    public static void swapBuffers(Object _this, long paramLong) {
        System.out.println("swapBuffers");
        gl.glEnable(GL_TEXTURE_2D);

        if (overlayBuffer == null) {
            int[] t = new int[1];
            gl.glGenTextures(1, t, 0);
            overlayTexture = t[0];
            gl.glBindTexture(GL_TEXTURE_2D, overlayTexture);
            gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
            gl.glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        } else
            gl.glBindTexture(GL_TEXTURE_2D, overlayTexture);

        if ((overlayBuffer == null || overlayBuffer.getWidth() != width || overlayBuffer.getHeight() != height) && width > 0 && height > 0) {
            WritableRaster raster = Raster.createInterleavedRaster(DataBuffer.TYPE_BYTE, width, height, 4, null);
            overlayBuffer = new BufferedImage(new ComponentColorModel(
                    ColorSpace.getInstance(ColorSpace.CS_sRGB),
                    new int[]{8, 8, 8, 8},
                    true,
                    false,
                    ComponentColorModel.TRANSLUCENT,
                    DataBuffer.TYPE_BYTE), raster, false, new Hashtable());
            overlayGraphics = overlayBuffer.createGraphics();
            overlayGraphics.setClip(0, 0, width, height);
        }

        byte[] buffer = ((DataBufferByte) overlayBuffer.getRaster().getDataBuffer()).getData();
        System.arraycopy(VBLANK, 0, buffer, 0, VBLANK.length);
        try {
            overlayGraphics.drawString("Hello!", 100, 100);
            executor.doPaint(overlayGraphics);
        } catch (Throwable error) {
            System.err.println("An error occurred while painting:");
            error.printStackTrace();
        }
        // Update the texture buffer
        gl.glTexImage2Dub(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, buffer, 0);
        // And blit
        gl.glColor4f(1, 1, 1, 1);
        gl.glBegin(GL_QUADS);
        gl.glTexCoord2f(0, 0);
        gl.glVertex2f(0, 0);
        gl.glTexCoord2f(0, 1);
        gl.glVertex2f(0, height);
        gl.glTexCoord2f(1, 1);
        gl.glVertex2f(width, height);
        gl.glTexCoord2f(1, 0);
        gl.glVertex2f(width, 0);
        gl.glEnd();
        bufferHook(_this);
        try {
            swapBuffers.invoke(_this, paramLong);
        } catch (Throwable throwable) {
            throwable.printStackTrace();
        }

        //System.out.println(OpenGLDetour.imageBuckets[ITEM_WIDTH][ITEM_HEIGHT].head + " inventory items drawn");
        imageBucketInvalidator.iter++;
//        if (executor != null) {
//            try {
//                // System.out.println(canvas + " :: " + canvas.hashCode());
//                executor.doPaint(overlayGraphics);
//            } catch (Throwable error) {
//                System.err.println("An error occurred while painting:");
//                error.printStackTrace();
//            }
//        }
//        try {
//            swapBuffers.invoke(_this, paramLong);
//        } catch (Throwable throwable) {
//            throwable.printStackTrace();
//        }
    }
}
