package simpleclient.client.hook.jaggl;

public class IncHeap<T> {
    public static interface NewInstance<T> {
        public T newInst();
    }

    public static class Invalidator {
        public long iter = Long.MIN_VALUE;
    }

    private final String id;
    private final NewInstance<T> inst;
    private final Invalidator inv;
    public Object[] heap = new Object[1];
    public int head;
    public long iter;

    public IncHeap(String id, NewInstance<T> inst, Invalidator inv) {
        this.id = id;
        this.inst = inst;
        this.inv = inv;
    }

    public T get(int x) {
        if (iter != inv.iter) {
            iter = inv.iter;
            reset();
        }
        if (head >= heap.length) {
            System.out.println(id + " grew to " + (heap.length + 3));
            Object[] _heap = new Object[heap.length + 3];
            System.arraycopy(heap, 0, _heap, 0, heap.length);
            head = heap.length;
            heap = _heap;
        }
        if (heap[head] == null)
            heap[head] = inst.newInst();
        return (T) heap[x];
    }

    public T next() {
        if (iter != inv.iter) {
            iter = inv.iter;
            reset();
        }
        if (head >= heap.length) {
            System.out.println(id + " grew to " + (heap.length + 3));
            Object[] _heap = new Object[heap.length + 3];
            System.arraycopy(heap, 0, _heap, 0, heap.length);
            head = heap.length;
            heap = _heap;
        }
        if (heap[head] == null)
            heap[head] = inst.newInst();
        return (T) heap[head++];
    }

    public void reset() {
       // System.out.println("Reset head");
        head = 0;
    }
}
