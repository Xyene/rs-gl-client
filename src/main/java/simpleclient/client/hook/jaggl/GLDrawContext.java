package simpleclient.client.hook.jaggl;

import java.util.Arrays;

class GLDrawContext {
    int vertexCount = 0;
    double textureSum = 0;
    double[] x = new double[4];
    double[] y = new double[4];
    int textureId;
    int bufferId;

    @Override
    public String toString() {
        return "GLDrawContext{" +
                "vertexCount=" + vertexCount +
                ", textureSum=" + textureSum +
                ", getXCoordinate=" + Arrays.toString(x) +
                ", y=" + Arrays.toString(y) +
                ", textureId=" + textureId +
                '}';
    }
}
