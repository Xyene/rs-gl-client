package simpleclient.client.hook.jaggl.draw;

import java.awt.*;

import static simpleclient.client.hook.jaggl.GLHookProvider.gl;


/**
 * @author hakan eryargi (r a f t)
 */
class OpenGL {
    static final void initDraw() {
        initProjection();
        initOrthoMode();
        disableTextures();
    }

    static final void endDraw() {
        endProjection();
        gl.glDisable(gl.GL_SCISSOR_TEST);
    }

    static final void initBlit() {
        initProjection();
        initOrthoMode();
        enableTextures();

        gl.glEnable(gl.GL_BLEND);
        gl.glBlendFunc(gl.GL_SRC_ALPHA, gl.GL_ONE_MINUS_SRC_ALPHA);
    }

    static final void endBlit() {
        gl.glMatrixMode(gl.GL_PROJECTION);
        gl.glPopMatrix();
        gl.glDisable(gl.GL_TEXTURE_2D);
        gl.glDisable(gl.GL_BLEND);
    }

    static void bindTexture(int textureId) {
        gl.glBindTexture(gl.GL_TEXTURE_2D, textureId);
    }

    static void enableTextures() {
        gl.glEnable(gl.GL_TEXTURE_2D);
    }

    static void disableTextures() {
        gl.glDisable(gl.GL_TEXTURE_2D);
    }

    static void setColor(Color color) {
        if (color != null)
            gl.glColor4f(color.getRed() / 255f, color.getGreen() / 255f,
                    color.getBlue() / 255f, color.getAlpha() / 255f);
    }

    static void initOrthoMode() {
//        int width = Display.getDisplayMode().getWidth();
//        int height = Display.getDisplayMode().getHeight();
//        gl.glOrtho(0, width, height, 0, -1.0, 1.0);
    }

    static void initProjection() {
        gl.glMatrixMode(gl.GL_PROJECTION);
        gl.glPushMatrix();
        gl.glLoadIdentity();
    }

    static void endProjection() {
        gl.glMatrixMode(gl.GL_PROJECTION);
        gl.glPopMatrix();
    }
}
