package simpleclient.client.hook.jaggl.draw;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

import static simpleclient.client.hook.jaggl.GLHookProvider.gl;

/**
 * <p>OpenGL texture</p>
 *
 * @author hakan eryargi (r a f t)
 */
public class GLTexture {
    private static final int NO_ID = -1;

    private final int width, height;
    private final int[] pixels;
    private int id = NO_ID;

    public GLTexture(Image image) {
        this(createBufferedImage(image));
    }

    public GLTexture(BufferedImage image) {
        assert isPowerOfTwo(image.getWidth()) : "width is not power of two: " + image.getWidth();
        assert isPowerOfTwo(image.getHeight()) : "hieght is not power of two: " + image.getHeight();

        image = convertImage(image);

        DataBufferInt dataBuffer = (DataBufferInt) (image.getRaster().getDataBuffer());
        this.pixels = dataBuffer.getData();
        this.width = image.getWidth();
        this.height = image.getHeight();
    }

    int getId() {
        return id;
    }

    /**
     * uploads texture to video card and binds it.
     */
    void bind() {
        if (id != NO_ID) // already bound
            return;

        ByteBuffer textureBuffer = ByteBuffer.allocateDirect(pixels.length << 2); // * 4
        textureBuffer.order(ByteOrder.LITTLE_ENDIAN);

        for (int i = 0; i < pixels.length; i++) {
            pixels[i] = correctPixel(pixels[i]);
        }

        int[] buf = new int[1];
        gl.glGenTextures(0, buf, 0);
        id = buf[0];

        gl.glBindTexture(gl.GL_TEXTURE_2D, id);

        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_MIN_FILTER, gl.GL_LINEAR);
        gl.glTexImage2Di(gl.GL_TEXTURE_2D, 0, gl.GL_RGBA, width, height, 0, gl.GL_RGBA, gl.GL_INT, pixels, 0);

        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_WRAP_S, gl.GL_CLAMP);
        gl.glTexParameteri(gl.GL_TEXTURE_2D, gl.GL_TEXTURE_WRAP_T, gl.GL_CLAMP);
    }

    private static int correctPixel(int pixel) {
        // swap red and blue: 0xaarrggbb -> 0xaabbggrr
        return (pixel & 0xff00ff00) | ((pixel & 0x00ff0000) >> 16) | ((pixel & 0x000000ff) << 16);
    }

    private static BufferedImage createBufferedImage(Image image) {
        if (image instanceof BufferedImage)
            return (BufferedImage) image;
        return copyImage(image);
    }

    private static BufferedImage convertImage(BufferedImage image) {
        if (image.getType() == BufferedImage.TYPE_INT_ARGB)
            return image;
        return copyImage(image);
    }

    private static BufferedImage copyImage(Image image) {
        BufferedImage buffImage = new BufferedImage(image.getWidth(null),
                image.getHeight(null), BufferedImage.TYPE_INT_ARGB);
        buffImage.getGraphics().drawImage(image, 0, 0, null);
        return buffImage;
    }

    private static boolean isPowerOfTwo(int value) {
        int i = 2;
        while (i < value)
            i <<= 1;
        return (i == value);
    }
}
