package simpleclient.client.hook.jaggl;

/* 
 * Copyright (c) 2002-2008 LWJGL Project
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are
 * met:
 *
 * * Redistributions of source code must retain the above copyright
 *   notice, this list of conditions and the following disclaimer.
 *
 * * Redistributions in binary form must reproduce the above copyright
 *   notice, this list of conditions and the following disclaimer in the
 *   documentation and/or other materials provided with the distribution.
 *
 * * Neither the getName of 'LWJGL' nor the names of
 *   its contributors may be used to endorse or promote products derived
 *   from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

public class GLU {
    private static final float[] in = new float[4];
    private static final float[] out = new float[4];
    private static final float[] tempMatrix = new float[16];
    private static final float[] IDENTITY_MATRIX =
            new float[]{
                    1.0f, 0.0f, 0.0f, 0.0f,
                    0.0f, 1.0f, 0.0f, 0.0f,
                    0.0f, 0.0f, 1.0f, 0.0f,
                    0.0f, 0.0f, 0.0f, 1.0f};

    private static void __gluMakeIdentityf(float[] m) {
        System.arraycopy(IDENTITY_MATRIX, 0, m, 0, 16);
    }

    private static boolean __gluInvertMatrixf(float[] src, float[] inverse) {
        int i, j, k, swap;
        float t;
        float[] temp = GLU.tempMatrix;

        System.arraycopy(src, 0, temp, 0, 16);

        __gluMakeIdentityf(inverse);

        for (i = 0; i < 4; i++) {
            /*
             * * Look for largest element in column
			 */
            swap = i;
            for (j = i + 1; j < 4; j++) {
				/*
				 * if (fabs(temp[j][i]) > fabs(temp[i][i])) { swap = j;
				 */
                if (Math.abs(temp[j * 4 + i]) > Math.abs(temp[i * 4 + i])) {
                    swap = j;
                }
            }

            if (swap != i) {
				/*
				 * * Swap rows.
				 */
                for (k = 0; k < 4; k++) {
                    t = temp[i * 4 + k];
                    temp[i * 4 + k] = temp[swap * 4 + k];
                    temp[swap * 4 + k] = t;

                    t = inverse[i * 4 + k];
                    inverse[i * 4 + k] = inverse[swap * 4 + k];
                    //inverse.put((i << 2) + k, inverse[(swap << 2) + k));
                    inverse[swap * 4 + k] = t;
                    //inverse.put((swap << 2) + k, t);
                }
            }

            if (temp[i * 4 + i] == 0) {
				/*
				 * * No non-zero pivot. The matrix is singular, which shouldn't *
				 * happen. This means the user gave us a bad matrix.
				 */
                return false;
            }

            t = temp[i * 4 + i];
            for (k = 0; k < 4; k++) {
                temp[i * 4 + k] = temp[i * 4 + k] / t;
                inverse[i * 4 + k] = inverse[i * 4 + k] / t;
            }
            for (j = 0; j < 4; j++) {
                if (j != i) {
                    t = temp[j * 4 + i];
                    for (k = 0; k < 4; k++) {
                        temp[j * 4 + k] = temp[j * 4 + k] - temp[i * 4 + k] * t;
                        inverse[j * 4 + k] = inverse[j * 4 + k] - inverse[i * 4 + k] * t;
						/*inverse.put(
							(j << 2) + k,
							inverse[(j << 2) + k) - inverse[(i << 2) + k) * t);*/
                    }
                }
            }
        }
        return true;
    }

    private static void __gluMultMatrixVecf(float[] m, float[] in, float[] out) {
        for (int i = 0; i < 4; i++) {
            out[i] =
                    in[0] * m[+0 * 4 + i]
                            + in[1] * m[+1 * 4 + i]
                            + in[2] * m[+2 * 4 + i]
                            + in[3] * m[+3 * 4 + i];

        }
    }



    private static void __gluMultMatricesf(float[] a, float[] b, float[] r) {
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                r[i * 4 + j] =
                        a[i * 4 + 0] * b[0 * 4 + j] + a[i * 4 + 1] * b[1 * 4 + j] + a[i * 4 + 2] * b[2 * 4 + j] + a[i * 4 + 3] * b[3 * 4 + j]
                ;
            }
        }
    }

    private static final float[] finalMatrix = new float[16];

    public static boolean gluUnProject(
            float winx,
            float winy,
            float winz,
            float[] modelMatrix,
            float[] projMatrix,
            float[] viewport,
            float[] obj_pos) {
        float[] in = GLU.in;
        float[] out = GLU.out;

        __gluMultMatricesf(modelMatrix, projMatrix, finalMatrix);

        if (!__gluInvertMatrixf(finalMatrix, finalMatrix))
            return false;

        in[0] = winx;
        in[1] = winy;
        in[2] = winz;
        in[3] = 1.0f;

        // Map x and y from window coordinates
        in[0] = (in[0] - viewport[0]) / viewport[2];
        in[1] = (in[1] - viewport[1]) / viewport[3];

        // Map to range -1 to 1
        in[0] = in[0] * 2 - 1;
        in[1] = in[1] * 2 - 1;
        in[2] = in[2] * 2 - 1;

        __gluMultMatrixVecf(finalMatrix, in, out);

        if (out[3] == 0.0)
            return false;

        out[3] = 1.0f / out[3];

        obj_pos[0] = out[0] * out[3];
        obj_pos[1] = out[1] * out[3];
        obj_pos[2] = out[2] * out[3];

        return true;
    }

    public static boolean gluProject(
            float objx,
            float objy,
            float objz,
            float[] modelMatrix,
            float[] projMatrix,
            int[] viewport,
            float[] win_pos) {

        float[] in = GLU.in;
        float[] out = GLU.out;

        in[0] = objx;
        in[1] = objy;
        in[2] = objz;
        in[3] = 1.0f;

        __gluMultMatrixVecf(modelMatrix, in, out);
        __gluMultMatrixVecf(projMatrix, out, in);

        if (in[3] == 0.0)
            return false;

        in[3] = (1.0f / in[3]) * 0.5f;

        // Map x, y and z to range 0-1
        in[0] = in[0] * in[3] + 0.5f;
        in[1] = in[1] * in[3] + 0.5f;
        in[2] = in[2] * in[3] + 0.5f;

        // Map x,y to viewport
        win_pos[0] = in[0] * viewport[2] + viewport[0];
        win_pos[1] = in[1] * viewport[3] + viewport[1];
        win_pos[2] = in[2];

        return true;
    }
}
