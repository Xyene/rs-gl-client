package simpleclient.client.hook.jaggl;

import simpleclient.client.hook.HookManager;
import simpleclient.client.hook.IHookProvider;
import simpleclient.client.hook.jaggl.override.RandomAccessFile;
import simpleclient.client.hook.jaggl.proxy.GL;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.List;

public class GLHookProvider implements IHookProvider {
    static {
        HookManager.registerDetour(OpenGLDetour.class);
       // HookManager.registerOverride(RandomAccessFile.class);
       // HookManager.registerOverride(Canvas.class);
    }

    static GLHookProvider instance;
    /**
     * Parent buffer for overlay.
     */
    static BufferedImage overlayBuffer;
    /**
     * Graphics object for overlay, used in {@link simpleclient.client.hook.HookManager.IHookExecutor#doPaint(java.awt.Graphics)}.
     */
    static Graphics2D overlayGraphics;

    public GLHookProvider() {
        assert instance != null;
        instance = this;
    }

    /**
     * Reference to image definition by texture getId.
     */
    static final GLTexture[] TEXTURES = new GLTexture[16384];
    /**
     * Width and height of all items rendered.
     */
    public static final int ITEM_WIDTH = 36, ITEM_HEIGHT = 32;
    /**
     * Proxy to jaggl.OpenGL
     */
    public static GL gl;
    /**
     * Inventory items currently visible.
     * Gets populated by (@link currentItems) when screen buffer is swapped.
     */
    static List<GLTexturedQuad> items = new ArrayList<>(32);
    static List<GLTexturedQuad> tabs = new ArrayList<>(32);
    static List<GLTexturedQuad> buttons = new ArrayList<>(32);

    /**
     * Models currently visible.
     * Gets populated by (@link currentModels) when screen buffer is swapped.
     */
    static List<GLModel> models = new ArrayList<>(32);
    /**
     * Callback provider
     */
    static HookManager.IHookExecutor executor;
    /**
     * Marker for whether this hook has been initialized.
     */
    static boolean hookEnabled;
    static int TABICON_WIDTH = 24, TABICON_HEIGHT = 24;
    static int ABILITY_ICON_WIDTH = 30, ABILITY_ICON_HEIGHT = 30;
    static int SETTINGS_BUTTON_WIDTH = 32, SETTINGS_BUTTON_HEIGHT = 32;

    static void ensureActive() {
        if (!hookEnabled)
            throw new IllegalStateException("OpenGL hook not active");
    }

    @Override
    public boolean isActive() {
        return hookEnabled;
    }

    @Override
    public void setHookExecutor(HookManager.IHookExecutor executor) {
        GLHookProvider.executor = executor;
    }
}
