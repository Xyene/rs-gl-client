package simpleclient.client.hook.jaggl.override;

import simpleclient.client.hook.ClassProxy;

import java.io.File;
import java.io.FileNotFoundException;

@ClassProxy("java/io/RandomAccessFile")
public class RandomAccessFile extends java.io.RandomAccessFile {
//
    public static String swap(String path) {
       File f = new File(path.replace("jagex", "xy"));
            f.mkdirs();
        return f.getPath();
    }

    public RandomAccessFile(String name, String mode) throws FileNotFoundException {
        super(name, mode);
        //System.out.println(mode + "> " + getName);
    }

    public RandomAccessFile(File file, String mode) throws FileNotFoundException {
        this(file.getPath(), mode);
    }
}
