package simpleclient.client.hook.jaggl.override;


import simpleclient.client.hook.ClassProxy;

import java.awt.*;
import java.awt.image.BufferedImage;

@ClassProxy("java/awt/Canvas")
public class RSCanvas extends Canvas {

    private static BufferedImage botBuffer = new BufferedImage(765, 503, BufferedImage.TYPE_INT_RGB);
    private BufferedImage gameBuffer = new BufferedImage(765, 503, BufferedImage.TYPE_INT_RGB);

    public RSCanvas() {
        System.out.println("!!!");
    }

    /**
     * Constructs a new Canvas given a GraphicsConfiguration object.
     *
     * @param config a reference to a GraphicsConfiguration object.
     * @see java.awt.GraphicsConfiguration
     */
    public RSCanvas(GraphicsConfiguration config) {
        this();
    }


    @Override
    public void paint(Graphics g) {
        System.out.println("paint");
    }

    @Override
    public void update(Graphics g) {
        System.out.println("update");
    }

}