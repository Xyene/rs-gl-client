package simpleclient.client.hook;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Indicates that classes marked with this annotation are containers for {@link Detour}
 * and {@link tk.ivybits.xybot.client.hook.Notifier} marked methods. This annotation specifies what class the latter
 * and former should be applied to.
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Interceptor {
    /**
     * Fetches the unqualified getName of the class to intercept.
     *
     * @return The getName of the unqualified class to intercept
     */
    String value();
}
