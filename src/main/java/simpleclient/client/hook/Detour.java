package simpleclient.client.hook;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Specifies a detour for a method.
 * Must be declared in a Class annotated with {@link Interceptor}, to define
 * which Class this detour belongs to.
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Detour {
    public enum TargetType {
        STATIC, INSTANCE
    }

    TargetType type() default TargetType.STATIC;

    /**
     *
     * The method to detour.
     * <p/>
     * Annotated methods must share the same signature (parameter types & return type) as
     * the method being detoured.
     *
     * @return The getName of the method being detoured: if left blank, will use the override method getName
     */
    String target() default "";
}
