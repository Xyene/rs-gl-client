package simpleclient.client.hook;

import java.util.List;

public interface IHookProvider {

    boolean isActive();

    void setHookExecutor(HookManager.IHookExecutor executor);
}
