package simpleclient.client;

import java.applet.AppletContext;
import java.applet.AppletStub;
import java.net.URL;
import java.util.HashMap;

public class ClientAppletStub implements AppletStub {
    private final URL base;
    private final HashMap<String, String> parameters;

    public ClientAppletStub(URL base, HashMap<String, String> parameters) {
        this.base = base;
        this.parameters = parameters;
    }

    @Override
    public boolean isActive() {
        return true;
    }

    @Override
    public URL getDocumentBase() {
        return base;
    }

    @Override
    public URL getCodeBase() {
        return base;
    }

    @Override
    public String getParameter(String name) {
        if("17".equals(name)) return null;
        return parameters.get(name);
    }

    @Override
    public AppletContext getAppletContext() {
        return null;
    }

    @Override
    public void appletResize(int width, int height) {

    }
}
