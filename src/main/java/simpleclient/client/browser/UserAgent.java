package simpleclient.client.browser;

import java.util.*;
import static java.util.AbstractMap.SimpleEntry;

public class UserAgent {
    public interface BrowserAgent {
        String getBrowser();
    }

    public static enum Platform {
        LINUX, WINDOWS, MAC;

        public static Platform getPlatform() {
            String os = System.getProperty("os.name").toLowerCase();
            if (os.contains("win")) {
                return WINDOWS;
            }
            if (os.contains("nix") || os.contains("nux") || os.contains("aix")) {
                return LINUX;
            }
            if (os.contains("mac")) {
                return MAC;
            }
            return null;
        }
    }

    private static HashMap<Platform, ArrayList<BrowserAgent>> browserAgents = new HashMap<>();

    public static void registerBrowser(BrowserAgent browser, Platform... platforms) {
        for (Platform platform : platforms) {
            if (!browserAgents.containsKey(platform))
                browserAgents.put(platform, new ArrayList<BrowserAgent>());
            browserAgents.get(platform).add(browser);
        }
    }

    static {
        registerBrowser(new BrowserAgent() {
            private List<String> versions = Arrays.asList("32.0.1667.0", "32.0.1664.3", "31.0.1650.16", "31.0.1623.0",
                    "30.0.1599.17", "29.0.1547.62", "29.0.1547.57", "29.0.1547.2", "28.0.1468.0", "28.0.1467.0",
                    "28.0.1464.0", "27.0.1453.93", "27.0.1453.116");
            @Override
            public String getBrowser() {
                return String.format("AppleWebKit/537.36 (KHTML, like Gecko) Chrome/%s Safari/537.36",
                        getRandomElement(versions));
            }
        }, Platform.WINDOWS, Platform.LINUX);
        registerBrowser(new BrowserAgent() {
            private List<SimpleEntry<String, String>> versions = Arrays.asList(
                    new SimpleEntry<>("20100101", "25.0"), new SimpleEntry<>("20100101", "24.0"),
                    new SimpleEntry<>("20130405", "23.0"), new SimpleEntry<>("20130405", "22.0"),
                    new SimpleEntry<>("20121011", "21.0.1"), new SimpleEntry<>("20121011", "21.0.0"));
            @Override
            public String getBrowser() {
                SimpleEntry<String, String> version = getRandomElement(versions);
                return String.format("Gecko/%s Firefox/%s", version.getKey(), version.getValue());
            }
        }, Platform.WINDOWS, Platform.LINUX);
    }

    public static <T> T getRandomElement(List<T> list) {
        return list.get(new Random().nextInt(list.size()));
    }

    public static String getSystemAgent() {
        switch (Platform.getPlatform()) {
            case WINDOWS:
                return String.format("Mozilla/5.0 (Windows NT %s; %srv:21.0)", System.getProperty("os.version"),
                        System.getProperty("os.arch").equals("x64") ? "WOW64; " : "");
            // TODO: more platforms
        }
        return "Mozilla/5.0";
    }

    public static String getUserAgent() {
        return getSystemAgent() + " " + getRandomElement(browserAgents.get(Platform.getPlatform())).getBrowser();
    }

    public static void main(String[] args) {
        for (Map.Entry entry : System.getProperties().entrySet()) {
            System.out.printf("%50s: %s\n", entry.getKey(), entry.getValue());
        }
        System.out.println();
        System.out.println("Accept Language: " + Browser.getAcceptLanguage());
        System.out.println();
        for (int i = 0; i < 20; ++i) {
            System.out.println("User Agent: " + getUserAgent());
        }
    }
}
