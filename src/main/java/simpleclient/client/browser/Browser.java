package simpleclient.client.browser;

import simpleclient.client.ClientAppletStub;
import simpleclient.client.loader.AppletClassLoader;

import java.applet.Applet;
import java.awt.*;
import java.io.*;
import java.net.JarURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

public class Browser {
    public static Browser newSession() {
        return new Browser();
    }

    public byte[] getBytes(String url) throws IOException {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        transfer(url, baos);
        return baos.toByteArray();
    }

    public String getContentResult(String url) throws IOException {
        return new String(getBytes(url));
    }

    private JarURLConnection requestApplet(String url) throws IOException {
        JarURLConnection clientConnection = (JarURLConnection) new URL("jar:" + url + "!/").openConnection();
        //This might need some work, I didn't write it and I'm not sure how accurate it is
        clientConnection.addRequestProperty("Protocol", "HTTP/1.1");
        clientConnection.addRequestProperty("Connection", "keep-alive");
        clientConnection.addRequestProperty("Keep-Alive", "200");
        //This useragent it for the java plugin, probably shouldn't mess with it
        clientConnection.addRequestProperty("User-Agent", "Mozilla/4.0 (" + System.getProperty("os.name") + " " + System.getProperty("os.version") + ") Java/" + System.getProperty("java.version"));
        return clientConnection;
    }

    public Applet getApplet(boolean oldschool, int world) throws IOException, ClassNotFoundException, IllegalAccessException, InstantiationException {
        String URL_BASE = String.format("http://%s%d.runescape.com/", oldschool ? "oldschool" : "world", world);
        System.err.println("Loading " + URL_BASE);
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        transfer(URL_BASE + "l=0/jav_config.ws", baos);
        BufferedReader is = new BufferedReader(new InputStreamReader(new ByteArrayInputStream(baos.toByteArray()), "UTF-8"));

        final HashMap<String, String> parameters = new HashMap<>();

        String line;
        String jar = null, initClass = null;
        int minWidth = 765, minHeight = 540;
        int maxWidth = 3200, maxHeight = 1200;
        while ((line = is.readLine()) != null) {
            if (line.startsWith("param=")) {
                String[] data = line.substring(6).split("=", 2);
                parameters.put(data[0], data[1]);
            } else if (line.startsWith("initial_jar")) {
                jar = line.substring(12);
            } else if (line.startsWith("initial_class")) {
                initClass = line.substring(14).replace(".class", "");
            } else if (line.startsWith("applet_minwidth")) {
                minWidth = Integer.parseInt(line.substring(16));
            } else if (line.startsWith("applet_minheight")) {
                minHeight = Integer.parseInt(line.substring(17));
            } else if (line.startsWith("applet_maxwidth")) {
                maxWidth = Integer.parseInt(line.substring(16));
            } else if (line.startsWith("applet_maxheight")) {
                maxHeight = Integer.parseInt(line.substring(17));
            }
        }

        if (jar == null)
            throw new ExceptionInInitializerError("failed to find archive");
        if (initClass == null)
            throw new ExceptionInInitializerError("failed to find code");

        AppletClassLoader loader = new AppletClassLoader(requestApplet(URL_BASE + jar).getJarFileURL());
        Applet applet = (Applet) loader.loadClass(initClass).newInstance();
        applet.setMinimumSize(new Dimension(minWidth, minHeight));
        applet.setMaximumSize(new Dimension(maxWidth, maxHeight));
        applet.setStub(new ClientAppletStub(new URL(URL_BASE), parameters));
        return applet;
    }

    public static String getAcceptLanguage() {
        String locale = Locale.getDefault().toString().replace("_", "-").toLowerCase();
        ArrayList<String> locales = new ArrayList<>();
        locales.add(locale);
        if (locale.contains("-"))
            locales.add(locale.split("-")[0]);
        if (!locale.equals("en"))
            locales.add("en");
        locale = "";
        for (String item : locales) {
            locale += item + ", ";
        }
        return locale.substring(0, locale.length() - 2);
    }

    public void transfer(String url, OutputStream out) throws IOException {
        URLConnection conch = new URL(url).openConnection();
        // Seems legit
        conch.setRequestProperty("Protocol", "HTTP/1.1");
        conch.setRequestProperty("Connection", "keep-alive");
        conch.setRequestProperty("Keep-Alive", "200");
        conch.setRequestProperty("User-Agent", UserAgent.getUserAgent());
        conch.setRequestProperty("Accept-Language", getAcceptLanguage());

        conch.connect();

        InputStream in = conch.getInputStream();

        byte[] chunk = new byte[4096];
        int bytesRead;

        while ((bytesRead = in.read(chunk)) > 0) {
            out.write(chunk, 0, bytesRead);
        }
    }
}
