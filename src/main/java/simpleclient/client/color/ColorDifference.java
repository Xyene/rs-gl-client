package simpleclient.client.color;

// this is color difference
// using method CIE94 from 1976

import java.util.zip.CRC32;

public class ColorDifference {
    private static CRC32 hasher = new CRC32();

    public static float round(float num, int multipleOf) {
        return (float) (Math.floor((num + (double) multipleOf / 2) / multipleOf) * multipleOf);
    }

    public static long genHash(int[] image, int width, int height) {
        if (image.length > 1) {
            float cx = ((float) width) / 2;
            float cy = ((float) height) / 2;
            float multiplier = 1;
            float max = (float) (Math.sqrt((0 - cx) * (0 - cx) + (0 - cy) * (0 - cy)) * multiplier);
            float avg = 0;

            byte[] binary = new byte[image.length / 2];
            int[] lab1 = new int[3], lab2 = new int[3];
            for (int i = 0; i < image.length - 2; i += 2) {
                RGBtoLAB(image[i], lab1);
                RGBtoLAB(image[i + 1], lab2);
                double diff = findDifference(lab1, lab2);
                //System.out.println(diff);
                //System.out.println(diff);
                binary[i / 2] = (byte) (diff > 1 ? 1 : 0);
                //int x = i % width;
                //int y = i / height;
                //float lum = max - (float) (diff * Math.sqrt((x - cx) * (x - cx) + (y - cy) * (y - cy)) * multiplier);
                // avg += lum;
            }
            //avg /= image.length / 2;

            hasher.update(binary);
            long hash = hasher.getValue();
            //hash += avg;
            //System.out.println(avg);
            hasher.reset();

            return hash;
        }
        return -1;
    }

    static public void RGBtoLAB(int argb, int[] lab) {
// this is the standard way to convert an int to argb value
        int r = (argb >> 16) & 0xFF;
        int g = (argb >> 8) & 0xFF;
        int b = (argb >> 0) & 0xFF;
        int a = (argb >> 24) & 0xFF;

        rgb2lab(a > 240 ? r : 0, a > 240 ? g : 0, a > 240 ? b : 0, lab);
    }

    public static void rgb2lab(int R, int G, int B, int[] lab) {
        //http://www.brucelindbloom.com

        float r, g, b, X, Y, Z, fx, fy, fz, xr, yr, zr;
        float Ls, as, bs;
        float eps = 216.f / 24389.f;
        float k = 24389.f / 27.f;

        float Xr = 0.964221f;  // reference white D50
        float Yr = 1.0f;
        float Zr = 0.825211f;

        // RGB to XYZ
        r = R / 255.f; //R 0..1
        g = G / 255.f; //G 0..1
        b = B / 255.f; //B 0..1

        // assuming sRGB (D65)
        if (r <= 0.04045)
            r = r / 12;
        else
            r = (float) Math.pow((r + 0.055) / 1.055, 2.4);

        if (g <= 0.04045)
            g = g / 12;
        else
            g = (float) Math.pow((g + 0.055) / 1.055, 2.4);

        if (b <= 0.04045)
            b = b / 12;
        else
            b = (float) Math.pow((b + 0.055) / 1.055, 2.4);


        X = 0.436052025f * r + 0.385081593f * g + 0.143087414f * b;
        Y = 0.222491598f * r + 0.71688606f * g + 0.060621486f * b;
        Z = 0.013929122f * r + 0.097097002f * g + 0.71418547f * b;

        // XYZ to Lab
        xr = X / Xr;
        yr = Y / Yr;
        zr = Z / Zr;

        if (xr > eps)
            fx = (float) Math.pow(xr, 1 / 3.);
        else
            fx = (float) ((k * xr + 16.) / 116.);

        if (yr > eps)
            fy = (float) Math.pow(yr, 1 / 3.);
        else
            fy = (float) ((k * yr + 16.) / 116.);

        if (zr > eps)
            fz = (float) Math.pow(zr, 1 / 3.);
        else
            fz = (float) ((k * zr + 16.) / 116);

        Ls = (116 * fy) - 16;
        as = 500 * (fx - fy);
        bs = 200 * (fy - fz);

        lab[0] = (int) (2.55 * Ls + .5);
        lab[1] = (int) (as + .5);
        lab[2] = (int) (bs + .5);
    }

    static public double findDifference(int argb1, int argb2) {
        int[] lab1 = new int[3];
        RGBtoLAB(argb1, lab1);
        int[] lab2 = new int[3];
        RGBtoLAB(argb2, lab2);
        return findDifference(lab1, lab2);
    }

    static public double findDifference(int[] lab1, int[] lab2) {
        double KL = 1;
        double K1 = 0.045;
        double K2 = 0.015;
// double KL = 2;
// double K1 = 0.048;
// double K2 = 0.014;

        double dL = lab1[0] - lab2[0];
        double c1 = Math.sqrt((Math.pow(lab1[1], 2) + Math.pow(lab1[2], 2)));
        double c2 = Math.sqrt((Math.pow(lab2[1], 2) + Math.pow(lab2[2], 2)));
        double dC = c1 - c2;
        double da = lab1[1] - lab2[1];
        double db = lab1[2] - lab2[2];
        double dH = Math.sqrt((Math.pow(da, 2) + Math.pow(db, 2) - Math.pow(dC, 2)));

        double sec1 = Math.pow((dL / KL), 2);
        double sec2 = Math.pow((dC / (1 + (K1 * c1))), 2);
        double sec3 = Math.pow((dH / (1 + (K2 * c1))), 2);

        return Math.sqrt(sec1 + sec2 + sec3);
    }
}