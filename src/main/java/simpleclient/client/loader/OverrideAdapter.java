package simpleclient.client.loader;

import simpleclient.client.hook.ClassProxy;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.commons.Remapper;
import org.objectweb.asm.commons.RemappingClassAdapter;

public class OverrideAdapter extends RemappingClassAdapter {
    private final Class hook;
    private String target;

    public OverrideAdapter(ClassVisitor classVisitor, final Class hook) {
        super(classVisitor, new Remapper() {
            @Override
            public String map(String s) {
                String target = ((ClassProxy) hook.getAnnotation(ClassProxy.class)).value();
                if (s.equals(target))
                    return hook.getName().replace(".", "/");
                return super.map(s);
            }
        });
        this.hook = hook;
    }
}
