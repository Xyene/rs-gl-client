package simpleclient.client.loader;

import simpleclient.client.hook.Proxy;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;

/**
 * Generates proxies to bind directly to static methods.
 * </p>
 * If the getName of a method is known beforehand, binding it with a proxy provides
 * better performance than using reflection.
 */
public class ProxyFactory implements Opcodes {
    private static final HashMap<Class, Object> PROXY_CACHE = new HashMap<>();

    private ProxyFactory() {
        throw new AssertionError();
    }

    /**
     * Generates a proxy instance for the given Class object.
     * The Class object must be annotated with {@link simpleclient.client.hook.Proxy}.
     * @param clazz The Class to generate a proxy for
     * @param <T> The type of the proxy
     * @return The generated proxy
     */
    public static <T> T getProxyFor(Class<T> clazz) {
        System.out.println("Proxying " + clazz + ": " + PROXY_CACHE);
        try {
          //  throw new Exception();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(PROXY_CACHE.containsKey(clazz))
            return (T) PROXY_CACHE.get(clazz);
        if (!clazz.isInterface())
            throw new IllegalArgumentException(clazz + " is not an interface");
        Proxy acc = clazz.getAnnotation(Proxy.class);
        if (acc == null)
            throw new IllegalArgumentException("@Proxy not declared for " + clazz);
        String hook = acc.value();

        final String implementation = clazz.getName().replace(".", "/") + "$" + System.identityHashCode(AppletClassLoader.getJagexClassLoader());
        ClassWriter cw = new ClassWriter(ClassWriter.COMPUTE_MAXS);
        MethodVisitor mv;

        cw.visit(V1_6,
                ACC_PUBLIC | ACC_SUPER,
                implementation,
                null,
                "java/lang/Object",
                new String[]{clazz.getName().replace(".", "/")});

        {
            mv = cw.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
            mv.visitVarInsn(ALOAD, 0);
            mv.visitMethodInsn(INVOKESPECIAL,
                    "java/lang/Object",
                    "<init>",
                    "()V");
            mv.visitInsn(RETURN);
            mv.visitMaxs(0, 0);
            mv.visitEnd();
        }

        for (Method call : clazz.getDeclaredMethods()) {
            if (!Modifier.isAbstract(call.getModifiers()))
                continue;
            String name = call.getName();
            String descriptor = Type.getMethodDescriptor(call);
            mv = cw.visitMethod(ACC_PUBLIC,
                    name,
                    descriptor,
                    null,
                    null);
            if (call.getParameterTypes().length > 0) {
                Type[] args = Type.getArgumentTypes(call);
                for (int idx = 0; idx != args.length; idx++) {
                    Type arg = args[idx];
                    mv.visitVarInsn(arg.getOpcode(ILOAD), idx + 1);
                }
            }
            mv.visitMethodInsn(INVOKESTATIC,
                    hook,
                    name,
                    descriptor);
            mv.visitInsn(Type.getReturnType(call).getOpcode(IRETURN));
            mv.visitMaxs(0, 0);
            mv.visitEnd();
        }
        cw.visitEnd();
        byte[] raw = cw.toByteArray();

        try {
            Method defineClass = ClassLoader.class.getDeclaredMethod("defineClass", String.class, byte[].class, int.class, int.class);
            defineClass.setAccessible(true);
            Object impl = ((Class) defineClass.invoke(AppletClassLoader.getJagexClassLoader(), implementation.replace("/", "."), raw, 0, raw.length)).newInstance();
            defineClass.setAccessible(false);
            PROXY_CACHE.put(clazz, impl);
            return (T) impl;
        } catch (ReflectiveOperationException e) {
            e.printStackTrace();
            return null;
        }
    }
}
