package simpleclient.client.loader;

import org.objectweb.asm.ClassAdapter;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodAdapter;
import org.objectweb.asm.MethodVisitor;

import static org.objectweb.asm.Opcodes.*;

/**
 * Hooks the ClassLoader used by Jagex to load classes post-decryption.
 * <p/>
 * Intercepted classes can be transformed, and other hooks added.
 */
public class ClassLoaderHookAdapter extends ClassAdapter {
    /**
     * Constructs a new ClassLoaderHookAdapter instance.
     *
     * @param classVisitor The ClassVisitor to chain to
     */
    public ClassLoaderHookAdapter(ClassVisitor classVisitor) {
        super(classVisitor);
    }

    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        super.visit(50, access, name, signature, superName, interfaces);

        // We only want to hook ClassLoaders
        if (superName.equals("java/lang/ClassLoader")) {
            MethodVisitor mv = super.visitMethod(ACC_PUBLIC, "<init>", "()V", null, null);
            mv.visitVarInsn(ALOAD, 0);
            // We must chain the system class loader, otherwise our hooks are invisible
            // Get the system ClassLoader: push it on stack
            mv.visitMethodInsn(
                    INVOKESTATIC,
                    "java/lang/ClassLoader",
                    "getSystemClassLoader",
                    "()Ljava/lang/ClassLoader;");
            // Call the super overload that takes a parent
            mv.visitMethodInsn(
                    INVOKESPECIAL,
                    "java/lang/ClassLoader",
                    "<init>",
                    "(Ljava/lang/ClassLoader;)V");
            mv.visitInsn(RETURN);
            mv.visitMaxs(0, 0);
            mv.visitEnd();
        }
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
        return new MethodAdapter(super.visitMethod(access, name, desc, signature, exceptions)) {
            public void visitMethodInsn(int opcode, String _owner, String _name, String _desc) {
                if (opcode == INVOKEVIRTUAL
                        && _name.equals("defineClass")
                        && _desc.equals("(Ljava/lang/String;[BIILjava/security/ProtectionDomain;)Ljava/lang/Class;")) {
                    // Push 'this' onto stack
                    super.visitVarInsn(ALOAD, 0);
                    super.visitMethodInsn(
                            INVOKESTATIC,
                            "simpleclient/client/loader/AppletClassLoader",
                            "__defineClass",
                            "(Ljava/lang/String;[BIILjava/security/ProtectionDomain;Ljava/lang/ClassLoader;)Ljava/lang/Class;");
                } else
                    super.visitMethodInsn(opcode, _owner, _name, _desc);
            }
        };
    }
}
