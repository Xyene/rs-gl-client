package simpleclient.client.loader;

import simpleclient.client.Reflection;
import simpleclient.client.hook.HookManager;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.ClassWriter;

import java.awt.*;
import java.io.*;
import java.net.SocketPermission;
import java.net.URL;
import java.net.URLClassLoader;
import java.security.CodeSource;
import java.security.Permissions;
import java.security.ProtectionDomain;
import java.security.cert.Certificate;
import java.util.Calendar;
import java.util.HashMap;
import java.util.PropertyPermission;

public class AppletClassLoader extends URLClassLoader {
    /**
     * We store the classes loaded from the Jagex ClassLoader in a cache.
     * The reasoning behind this is to provide an API similar to Class.forName:
     * the latter fails due to ClassLoader visibility issues.
     */
    private static final HashMap<String, Class> LOADED = new HashMap<>();
    /**
     * Reference the the Jagex ClassLoader, for use if a class ever needs to be loaded
     * into the RuneScape applet.
     */
    private static ClassLoader jagexClassLoader;

    public AppletClassLoader(URL... urls) {
        super(urls);
    }

    /**
     * Fetches the ClassLoader that RuneScape's encrypted inner.pack.gz is loaded by.
     *
     * @return The aforementioned ClassLoader
     */
    public static ClassLoader getJagexClassLoader() {
        return jagexClassLoader;
    }

    /**
     * Fetches a Class loaded by the Jagex ClassLoader {@see tk.ivybits.xybot.client.AppletClassLoader#getJagexClassLoader()}.
     * Similar to {@link Class#forName(String)}, except the Class will not be loaded
     * if it is not already.
     *
     * @param name The getName of the class to fetch.
     * @return The Class, or null if it hasn't been loaded yet or doesn't exist.
     */
    public static Class getJagexClass(String name) {
        return LOADED.get(name);
    }


    /**
     * ClassLoader hook to process loading RuneScape classes. This method should not be called directly.
     *
     * @param name
     * @param buffer
     * @param off
     * @param len
     * @param protectionDomain
     * @param from
     * @return
     */
    public static Class __defineClass(String name, byte[] buffer, int off, int len, ProtectionDomain protectionDomain, ClassLoader from) {
        if (jagexClassLoader == null)
            jagexClassLoader = from;
        ClassReader reader = new ClassReader(buffer, off, len);
        ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_MAXS);
        // Add the class adapter as a modifier
        // We skip frames since ASM wants to load the class to get the needed common superclass: causes crash
        ClassVisitor visitor = new ClassLoaderHookAdapter(writer);
        for (Class detour : HookManager.getAllDetours()) {
            visitor = new InterceptAdapter(visitor, detour);
        }
        for (Class override : HookManager.getAllOverrides()) {
            visitor = new OverrideAdapter(visitor, override);
        }

        reader.accept(visitor, ClassReader.SKIP_FRAMES);
        // System.out.println("Found client class");
        if(false)
        try {
            File f = new File("client6/" + name + ".class");
            new File("client6").mkdir();
            f.createNewFile();
            FileOutputStream fos = new FileOutputStream("client6/" + name + ".class");
            fos.write(buffer);
            fos.close();
        } catch (Exception ne) {
            ne.printStackTrace();
        }
      //  if (getName.equals("client"))
        //    reader.accept(new VersionVisitor(), ClassReader.SKIP_FRAMES);

        buffer = writer.toByteArray();

        try {
            Class c = Reflection.declaredMethod("defineClass")
                    .in(from)
                    .in(ClassLoader.class)
                    .withParameters(String.class, byte[].class, int.class, int.class, ProtectionDomain.class)
                    .withReturnType(Class.class)
                    .invoke(name, buffer, 0, buffer.length, protectionDomain);
            LOADED.put(name, c);
            return c;
        } catch (Exception ignored) {
            ignored.printStackTrace();
        }
        return null;
    }

    @Override
    protected Class loadClass(String name, boolean resolve) throws ClassNotFoundException {
        if (name.startsWith("java.") ||
                name.startsWith("javax.") ||
                name.startsWith("org.objectweb.asm.") ||
                name.startsWith("io.xy.")) {
            // Only bootstrap class loader can define classes in java.*, javax.* uses its own for some
            // We just don't need to process ASM, or ourselves.
            // System.out.println("Loaded from Jagex CLoader: " + getName);
            return super.loadClass(name, resolve);
        }

        try {
            ByteArrayOutputStream bs = new ByteArrayOutputStream();
            InputStream is = getResourceAsStream(name.replace('.', '/') + ".class");
            byte[] buf = new byte[65535];
            int len;
            while ((len = is.read(buf)) > 0) {
                bs.write(buf, 0, len);
            }
            byte[] buffer = bs.toByteArray();

            // Create class reader from buffer
            ClassReader reader = new ClassReader(buffer);
            // Make writer
            ClassWriter writer = new ClassWriter(ClassWriter.COMPUTE_MAXS);

            // Add the class adapter as a modifier
            reader.accept(new ClassLoaderHookAdapter(writer), ClassReader.SKIP_FRAMES);
            buffer = writer.toByteArray();

            final Permissions ps = new Permissions();
            ps.add(new AWTPermission("accessEventQueue"));
            ps.add(new PropertyPermission("user.home", "read"));
            ps.add(new PropertyPermission("java.vendor", "read"));
            ps.add(new PropertyPermission("java.version", "read"));
            ps.add(new PropertyPermission("os.title", "read"));
            ps.add(new PropertyPermission("os.arch", "read"));
            ps.add(new PropertyPermission("os.version", "read"));
            ps.add(new SocketPermission("*", "connect,resolve"));
            // ps.add(new AllPermission());
            String uDir = System.getProperty("user.home");
            if (uDir != null) {
                uDir += "/";
            } else {
                uDir = "~/";
            }
            final String[] dirs = {"c:/rscache/", "/rscache/", "c:/windows/", "c:/winnt/", "c:/", uDir, "/tmp/", "."};
            final String[] rsDirs = {".jagex_cache_32", ".file_store_32"};
            for (String dir : dirs) {
                final File f = new File(dir);
                ps.add(new FilePermission(dir, "read"));
                if (!f.exists()) {
                    continue;
                }
                dir = f.getPath();
                for (final String rsDir : rsDirs) {
                    ps.add(new FilePermission(dir + File.separator + rsDir + File.separator + "-", "read"));
                    ps.add(new FilePermission(dir + File.separator + rsDir + File.separator + "-", "write"));
                }
            }
            Calendar.getInstance();
//TimeZone.getDefault();//Now the default is set they don't need permission
//ps.add(new FilePermission())
            ps.setReadOnly();
            return defineClass(
                    name,
                    buffer,
                    0,
                    buffer.length,
                    new ProtectionDomain(
                            new CodeSource(null, (Certificate[]) null),
                            ps));
        } catch (Exception e) {
            e.printStackTrace();
            return super.loadClass(name, resolve);
        }
    }
}
