package simpleclient.client.loader;

import simpleclient.client.hook.Detour;
import simpleclient.client.hook.Interceptor;
import org.objectweb.asm.ClassAdapter;
import org.objectweb.asm.ClassVisitor;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.commons.GeneratorAdapter;

import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.HashMap;

import static simpleclient.client.hook.Detour.TargetType.STATIC;
import static org.objectweb.asm.Opcodes.INVOKESTATIC;

public class InterceptAdapter extends ClassAdapter {
    private HashMap<String, String> staticDetours = new HashMap<>();
    private HashMap<String, String> instanceDetours = new HashMap<>();
    private final Class hook;
    private final String target;

    public InterceptAdapter(ClassVisitor classVisitor, Class hook) {
        super(classVisitor);
        this.hook = hook;
        for (Method possible : hook.getDeclaredMethods()) {
            Detour intercept = possible.getAnnotation(Detour.class);
            if (intercept != null) {
                if (Modifier.isStatic(possible.getModifiers())) {
                    String target = intercept.target();
                    (intercept.type() == Detour.TargetType.STATIC ? staticDetours : instanceDetours).put(target.isEmpty() ? possible.getName() : target, possible.getName());
                } else {
                    throw new IllegalArgumentException(possible + " not static");
                }
            }
        }
        target = ((Interceptor) hook.getAnnotation(Interceptor.class)).value();
    }

    @Override
    public void visit(int version, int access, String name, String signature, String superName, String[] interfaces) {
        super.visit(50, access, name, signature, superName, interfaces);
    }

    @Override
    public MethodVisitor visitMethod(int access, String name, String desc, String signature, String[] exceptions) {
        return new GeneratorAdapter(super.visitMethod(access, name, desc, signature, exceptions), access, name, desc) {
            public void visitMethodInsn(int opcode,
                                        String owner,
                                        String name,
                                        String desc) {
                if (owner.equals(target)) {
                    if (opcode == INVOKESTATIC && staticDetours.containsKey(name)) {
                        super.visitMethodInsn(
                                INVOKESTATIC,
                                hook.getName().replace(".", "/"),
                                staticDetours.get(name),
                                desc);
                        //System.out.printf("Routing static %s -> %s\n", getName, hook.getName());
                        return;
                    } else if (instanceDetours.containsKey(name)) {
                        desc = "(Ljava/lang/Object;" + desc.substring(1);
                        System.out.println(target + ">" + name);
                        super.visitMethodInsn(
                                INVOKESTATIC,
                                hook.getName().replace(".", "/"),
                                instanceDetours.get(name),
                                desc);
                        //System.out.printf("Routing instance %s -> %s\n", getName, hook.getName());
                        return;
                    }
                }
                super.visitMethodInsn(opcode, owner, name, desc);
            }
        };
    }
}
